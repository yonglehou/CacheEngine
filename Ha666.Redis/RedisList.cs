﻿using System.Collections.Generic;

namespace Ha666.Redis
{
    public abstract class RedisList<T>
    {
        public RedisList(string key, DataType dataType)
        {
            mDataType = dataType;
            mDataKey = key;
        }

        private string mDataKey;

        private DataType mDataType;

        #region lst

        public int Count(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.ListLength((string)mDataKey, serverid);
        }

        public T Pop(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.ListPop<T>((string)mDataKey, mDataType, serverid);
        }

        public T Remove(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.ListRemove<T>((string)mDataKey, mDataType, serverid);
        }

        public void Add(int serverid, T value)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.ListAdd((string)mDataKey, value, mDataType, serverid);
        }

        public void Push(int serverid, T value)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.ListPush((string)mDataKey, value, mDataType, serverid);
        }

        public IList<T> Range(int serverid, int start, int end)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.ListRange<T>((string)mDataKey, start, end, mDataType, serverid);
        }

        public IList<T> Range(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.ListRange<T>((string)mDataKey, mDataType, serverid);
        }

        public T GetItem(int serverid, int index)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.GetListItem<T>((string)mDataKey, index, mDataType, serverid);
        }

        public void SetItem(int serverid, int index, object value)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.SetListItem((string)mDataKey, index, value, mDataType, serverid);
        }

        public void Clear(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.Delete(serverid, mDataKey);
        }

        #endregion

    }
    public class StringList : RedisList<string>
    {
        public StringList(string key)
            : base(key, DataType.String)
        {
        }
    }

    public class ProtobufList<T> : RedisList<T>
    {
        public ProtobufList(string key)
            : base(key, DataType.Protobuf)
        {
        }
    }
}
