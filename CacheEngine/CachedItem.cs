﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CacheEngine
{
    /// <summary>缓存对像</summary>
    public class CachedItem
    {
        /// <summary>缓存对像</summary>
        public object Item { get; set; }

        /// <summary>过期时间</summary>
        public DateTime ExpirationDate { get; set; }


        /// <summary>默认构造</summary>
        /// <param name="item">缓存对像</param>
        /// <param name="expirationDate">过期时间</param>
        public CachedItem(object item, DateTime expirationDate)
        {
            Item = item;
            ExpirationDate = expirationDate;
        }
    }
}
