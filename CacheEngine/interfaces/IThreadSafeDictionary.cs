﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CacheEngine
{
    /// <summary>基于线程安全的字典</summary>
    /// <typeparam name="TKey">字典中键的类型</typeparam>
    /// <typeparam name="TValue">字典中值的类型</typeparam>
    internal interface IThreadSafeDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        /// <summary></summary>
        /// <param name="key"></param>
        /// <param name="newValue"></param>
        void MergeSafe(TKey key, TValue newValue);

        /// <summary></summary>
        /// <param name="key"></param>
        void RemoveSafe(TKey key);
    }
}
