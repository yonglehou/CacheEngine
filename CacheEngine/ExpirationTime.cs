﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CacheEngine
{
    /// <summary>过期时间</summary>
    public class ExpirationTime : IExpirationTime
    {
        private DateTime ExpirationDate { get; set; }

        /// <summary>默认构造</summary>
        /// <param name="expirationDate">过期时间</param>
        public ExpirationTime(DateTime expirationDate)
        {
            ExpirationDate = expirationDate;
        }
        /// <summary>取得过期时间</summary>
        /// <returns></returns>
        public DateTime GetExpirationDate()
        {
            return ExpirationDate;
        }

        /// <summary>设置过期时间 以小时间为单位</summary>
        /// <param name="hours">多少小时</param>
        /// <returns></returns>
        public static IExpirationTime HoursFromNow(float hours)
        {
            return new ExpirationTime(DateTime.Now.AddHours(hours));
        }

        /// <summary>不过期</summary>
        /// <returns></returns>
        public static IExpirationTime NeverExprire()
        {
            return new ExpirationTime(DateTime.MaxValue);
        }

        /// <summary>过期时间</summary>
        /// <param name="seconds">以秒为单位</param>
        /// <returns></returns>
        public static IExpirationTime SecondsFromNow(int seconds)
        {
            return HoursFromNow(seconds / 60 / 60);
        }
    }
}
