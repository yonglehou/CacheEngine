﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CacheEngine
{
    /// <summary>关键字生成器</summary>
    public class HashCodeKeyGenerator : IKeyGenerator
    {
        /// <summary>生成关键字</summary>
        /// <param name="objectType"></param>
        /// <param name="methodInfo"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public string GenerateKey(Type objectType, MethodInfo methodInfo, object[] args)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(objectType.FullName).Append("_");
            builder.Append(methodInfo.Name);

            if (args != null)
            {
                foreach (var o in args)
                {
                    if (o != null)
                        builder.Append("_").Append(o.GetHashCode());
                    else
                        builder.Append("_").Append("null");
                }
            }

            return builder.ToString();
        }
    }
}
